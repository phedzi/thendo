﻿
using Microsoft.EntityFrameworkCore;
using SaleAI.Models;

namespace SaleAI.Data
{
    public class StoreDBContext : DbContext
    {
        public StoreDBContext(DbContextOptions<StoreDBContext> options)
            :base(options)
        {
                
        }

        public DbSet<Store> TStore { get; set; }
        
        public DbSet<User> TUser { get; set; }
        public DbSet<Customer> TCustomer { get; set; }
        public DbSet<Administrator> TAdministrator { get; set; }
        public DbSet<SuperAdmin> TSuperAdmin { get; set; }
        public DbSet<UserLogIn> TUserLogIn { get; set; }
        public DbSet<Category> TCategory { get; set; }
        public DbSet<ProductType> TProductType { get; set; }
        public DbSet<Product> TProduct { get; set; }
        public DbSet<UserProduct> TUserProduct { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.SeedProductTypes(modelBuilder);
            this.BuildUserProduct(modelBuilder);
            this.SeedCategory(modelBuilder);
        }
        private void BuildUserProduct(ModelBuilder builder)
        {
            builder.Entity<UserProduct>().HasKey(bc => new { bc.UserId, bc.ProductId });

            builder.Entity<UserProduct>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.UserProducts)
                .HasForeignKey(bc => bc.UserId);
            builder.Entity<UserProduct>()
                .HasOne(bc => bc.Product)
                .WithMany(c => c.UserProducts)
                .HasForeignKey(bc => bc.ProductId);
        }

        private void SeedProductTypes(ModelBuilder builder)
        {
            builder.Entity<ProductType>().HasData(
                new ProductType() { ProductTypeId = 1, Type = "On Special", Deletable = false },
                new ProductType() { ProductTypeId = 2, Type = "Dashboards", Deletable = false });
        }
        private void SeedCategory(ModelBuilder builder)
        {
            builder.Entity<Category>().HasData(
                new Category() { CategoryId = 1, Name = "My Shopping List", Deletable = false });
        }
    }
}
