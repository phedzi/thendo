﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AIStore.Migrations
{
    public partial class changeModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TProductType",
                keyColumn: "ProductTypeId",
                keyValue: 2,
                column: "Type",
                value: "Dashboards");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "TProductType",
                keyColumn: "ProductTypeId",
                keyValue: 2,
                column: "Type",
                value: "Normal Pricing");
        }
    }
}
