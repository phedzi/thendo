﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AIStore.Migrations
{
    public partial class CreateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TCategory",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Selected = table.Column<bool>(nullable: false),
                    Deletable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TCategory", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "TProductType",
                columns: table => new
                {
                    ProductTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true),
                    Deletable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProductType", x => x.ProductTypeId);
                });

            migrationBuilder.CreateTable(
                name: "TStore",
                columns: table => new
                {
                    StoreId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    Views = table.Column<int>(nullable: false),
                    SpecialsLink = table.Column<string>(nullable: true),
                    SearchLink = table.Column<string>(nullable: true),
                    Theme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TStore", x => x.StoreId);
                });

            migrationBuilder.CreateTable(
                name: "TUser",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatetedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUser", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "TUserType",
                columns: table => new
                {
                    UserTypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Descriprion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUserType", x => x.UserTypeID);
                });

            migrationBuilder.CreateTable(
                name: "TProduct",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<string>(nullable: true),
                    PriceValue = table.Column<float>(nullable: false),
                    Link = table.Column<string>(nullable: true),
                    SmImg = table.Column<string>(nullable: true),
                    Img = table.Column<string>(nullable: true),
                    TypeProductTypeId = table.Column<int>(nullable: true),
                    StoreId = table.Column<int>(nullable: true),
                    Likes = table.Column<int>(nullable: false),
                    IsImage = table.Column<bool>(nullable: false),
                    CategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProduct", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_TProduct_TCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "TCategory",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TProduct_TStore_StoreId",
                        column: x => x.StoreId,
                        principalTable: "TStore",
                        principalColumn: "StoreId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TProduct_TProductType_TypeProductTypeId",
                        column: x => x.TypeProductTypeId,
                        principalTable: "TProductType",
                        principalColumn: "ProductTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TUserUserType",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    UserTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUserUserType", x => new { x.UserId, x.UserTypeId });
                    table.ForeignKey(
                        name: "FK_TUserUserType_TUser_UserId",
                        column: x => x.UserId,
                        principalTable: "TUser",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TUserUserType_TUserType_UserTypeId",
                        column: x => x.UserTypeId,
                        principalTable: "TUserType",
                        principalColumn: "UserTypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TUserProduct",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUserProduct", x => new { x.UserId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_TUserProduct_TProduct_ProductId",
                        column: x => x.ProductId,
                        principalTable: "TProduct",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TUserProduct_TUser_UserId",
                        column: x => x.UserId,
                        principalTable: "TUser",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TCategory",
                columns: new[] { "CategoryId", "Deletable", "Name", "Selected" },
                values: new object[] { 1, false, "My Shopping List", false });

            migrationBuilder.InsertData(
                table: "TProductType",
                columns: new[] { "ProductTypeId", "Deletable", "Type" },
                values: new object[,]
                {
                    { 1, false, "On Special" },
                    { 2, false, "Normal Pricing" }
                });

            migrationBuilder.InsertData(
                table: "TUserType",
                columns: new[] { "UserTypeID", "Descriprion", "Name" },
                values: new object[,]
                {
                    { 1, "Any user who would like to user the application", "User" },
                    { 2, "Anyone with SuperAdministrator rights and can delete/or block Administrators also Add", "Administrator" },
                    { 3, "Anyone with Administrative rights and Added by SuperAdministrator", "SuperAdmin" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_TProduct_CategoryId",
                table: "TProduct",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_TProduct_StoreId",
                table: "TProduct",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_TProduct_TypeProductTypeId",
                table: "TProduct",
                column: "TypeProductTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TUserProduct_ProductId",
                table: "TUserProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_TUserUserType_UserTypeId",
                table: "TUserUserType",
                column: "UserTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TUserProduct");

            migrationBuilder.DropTable(
                name: "TUserUserType");

            migrationBuilder.DropTable(
                name: "TProduct");

            migrationBuilder.DropTable(
                name: "TUser");

            migrationBuilder.DropTable(
                name: "TUserType");

            migrationBuilder.DropTable(
                name: "TCategory");

            migrationBuilder.DropTable(
                name: "TStore");

            migrationBuilder.DropTable(
                name: "TProductType");
        }
    }
}
