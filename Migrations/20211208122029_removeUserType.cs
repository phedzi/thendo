﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AIStore.Migrations
{
    public partial class removeUserType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TUserUserType");

            migrationBuilder.DropTable(
                name: "TUserType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TUserType",
                columns: table => new
                {
                    UserTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descriprion = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUserType", x => x.UserTypeId);
                });

            migrationBuilder.CreateTable(
                name: "TUserUserType",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    UserTypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUserUserType", x => new { x.UserId, x.UserTypeId });
                    table.ForeignKey(
                        name: "FK_TUserUserType_TUser_UserId",
                        column: x => x.UserId,
                        principalTable: "TUser",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TUserUserType_TUserType_UserTypeId",
                        column: x => x.UserTypeId,
                        principalTable: "TUserType",
                        principalColumn: "UserTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TUserType",
                columns: new[] { "UserTypeId", "Descriprion", "Name" },
                values: new object[] { 1, "Any user who would like to user the application", "User" });

            migrationBuilder.InsertData(
                table: "TUserType",
                columns: new[] { "UserTypeId", "Descriprion", "Name" },
                values: new object[] { 2, "Anyone with SuperAdministrator rights and can delete/or block Administrators also Add", "Administrator" });

            migrationBuilder.InsertData(
                table: "TUserType",
                columns: new[] { "UserTypeId", "Descriprion", "Name" },
                values: new object[] { 3, "Anyone with Administrative rights and Added by SuperAdministrator", "SuperAdmin" });

            migrationBuilder.CreateIndex(
                name: "IX_TUserUserType_UserTypeId",
                table: "TUserUserType",
                column: "UserTypeId");
        }
    }
}
