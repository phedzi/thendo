﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AIStore.Migrations
{
    public partial class updateUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserTypeID",
                table: "TUserType",
                newName: "UserTypeId");

            migrationBuilder.AddColumn<bool>(
                name: "RequestedPasswordReset",
                table: "TUser",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequestedPasswordReset",
                table: "TUser");

            migrationBuilder.RenameColumn(
                name: "UserTypeId",
                table: "TUserType",
                newName: "UserTypeID");
        }
    }
}
