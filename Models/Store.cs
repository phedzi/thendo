﻿
using System.Collections.Generic;

namespace SaleAI.Models
{
    public class Store
    {
        public int StoreId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Logo { get; set; }
        public int Views { get; set; }
        public string SpecialsLink { get; set; }
        public string SearchLink { get; set; }
        public string Theme { get; set; }
        public List<Product> Products { get; set; }

        public Store()
        {
            Name = "";
            Link = "";
            Logo = "";
            Theme = "";
            SearchLink = "";
            SpecialsLink = "";
            Products = new List<Product>();

        }
    }
}
