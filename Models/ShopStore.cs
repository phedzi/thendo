﻿using System.Collections.Generic;

namespace SaleAI.Models
{
    public class ShopStore
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public string specialLink { get; set; }
        public string searchLink { get; set; }
        public string logo { get; set; }
        public bool hasSpecials { get; set; }
        public string message { get; set; }
        public string theme { get; set; } = "red";
        public List<Product> products { get; set; } = new List<Product>();

        public ShopStore()
        {
            this.Id = 0;
            this.hasSpecials = true;
            this.specialLink = " ";
            this.searchLink = " ";
            this.message = " ";
            this.name = " ";
            this.link = " ";
            this.logo = " ";
            this.theme = "red";
            this.products = new List<Product>();
        }

        public ShopStore(int Id=0,string name = " ", string link = " ",string SpecialLnk= " ",string searchLink=" ", string logo = " ", string theme = " ", List<Product> products = null,bool hasSpecial=true,string message="" )
        {
            this.Id = Id;
            this.hasSpecials = hasSpecial;
            this.message = message;
            this.name = name;
            this.link = link;
            this.logo = logo;
            this.theme = theme;
            this.specialLink = SpecialLnk;
            this.searchLink = searchLink;
            this.products = products;
        }
    }
}
