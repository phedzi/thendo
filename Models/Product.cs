﻿using System.Collections.Generic;

namespace SaleAI.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public float PriceValue { get; set; }
        public string Link { get; set; }
        public string SmImg { get; set; }
        public int Quantity { get; set; }
        public string Img { get; set; }
        public ProductType Type { get; set; }
        public Store Store { get; set; }
        public int Likes { get; set; }
        public string Selected { get; set; } = "not-in-list";
        public bool IsImage { get; set; }
        public Category Category { get; set; }
        public List<UserProduct> UserProducts { get; set; } 

    }
}
