﻿using System;
using System.Collections.Generic;

namespace SaleAI.Models
{
    public class User 
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Discriminator { get; set; }
        public float Budget { get; set; }
        public bool RequestedPasswordReset { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAt { get; set; } 
        public DateTime UpdatetedAt { get; set; }
        public List<UserLogIn> UserLogIns { get; set; }
        public List<UserProduct> UserProducts { get; set; }
    }
}
