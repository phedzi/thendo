﻿using System;
using System.Collections.Generic;

namespace SaleAI.Models
{
    public class UserLogIn
    {
        public int Id { get; set; }
        public User User { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
