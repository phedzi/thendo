﻿
using System.Collections.Generic;

namespace SaleAI.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
        public bool Deletable { get; set; }
        public List<Product> Products { get; set; }
    }
}
