﻿using System.Collections.Generic;

namespace SaleAI.Models
{
    public class PropertiesResponse
    {
        public List<Category> Categorylist { get; set; }
        public List<ProductType> ProductTypeList { get; set; }
        public List<Store> StoreList { get; set; }

        public int UsersCount { get; set; }
        public int ShopsCount { get; set; }
        public int CustomersCount { get; set; }
        public int AdminCount { get; set; }

        public PropertiesResponse()
        {
            Categorylist = new List<Category>();
            ProductTypeList = new List<ProductType>();
            StoreList = new List<Store>();
        }
    }
}
