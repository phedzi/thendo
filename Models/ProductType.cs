﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.Models
{
    public class ProductType
    {
        public int ProductTypeId { get; set; }
        public string Type { get; set; }
        public bool Deletable { get; set; }

    }
}
