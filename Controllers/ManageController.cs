﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaleAI.BLL;
using SaleAI.ViewModels;

namespace SaleAI.Controllers
{
    public class ManageController : Controller
    {
        private readonly IUserService userService;
        private string SessionUserName = "_UserName";
        private string SessionName = "_Name";
        private string SessionUserType = "UserType";
        private LoggedInUserViewModel user;

        public ManageController(IUserService userService)
        {
            this.userService = userService;

        }

        [HttpGet]
        public IActionResult UserList()
        {
            GetCurrentUser();
            if (user == null || user.FirstName == null || this.user.UserType.Equals("Customer"))
                return RedirectToAction("Logout", "Account");

            ViewBag.User = user;

            var filter = HttpContext.Session.GetString("filter");
            if(filter != null)
                ViewBag.SelectedFilter = filter;
            else
                ViewBag.SelectedFilter = "ALL";

            var users = userService.GetReport(filter);
            return View(users);
        }

        [HttpPost]
        public IActionResult UserList(string filterType)
        {
            HttpContext.Session.SetString("filter", filterType);
            return RedirectToAction("UserList");
        }

        [HttpGet]
        public async Task<IActionResult> UserDetails(string id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            ViewBag.User = this.user;
            var user = await userService.ById(System.Int32.Parse(id));

            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {id} cannot be found";
                return View("NotFound");
            }

            var model = await userService.UserDetails(user);

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateUser([FromForm]string rdUserType, [FromForm]string userId)
        {
            var id = Int32.Parse(userId);
            var use = rdUserType;
            var user = await userService.ChangeUserType(id,rdUserType);
            GetCurrentUser();
            return RedirectToAction("UserList", "Manage");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            ViewBag.User = this.user;
            var user = await userService.ById(System.Int32.Parse(userId));

            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                return View("NotFound");
            }
            else
            {
                var result = await userService.Delete(user);
                if (result != null)
                    return RedirectToAction("UserList");


                return View("UserList");
            }
        }
        public void GetCurrentUser()
        {
            this.user = new LoggedInUserViewModel()
            {
                UserName = HttpContext.Session.GetString(SessionUserName),
                FirstName = HttpContext.Session.GetString(SessionName),
                UserType = HttpContext.Session.GetString(SessionUserType),
            };

        }

        [HttpPost]
        public async Task<IActionResult> ExportUsers()
        {
            var dictioneryexportType = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
            var expType = dictioneryexportType["exportType"];

            var filter = HttpContext.Session.GetString("filter");

            if(expType.Trim().ToLower() != ("selectexporttype"))
            {
                var content = await userService.ExportData(expType, filter);

                switch (expType)
                {
                    case "xlsx":
                        {
                            Response.Clear();
                            Response.Headers.Add("content-disposition", "attachment;filename=users_excel.xlsx");
                            Response.ContentType = "application/xlsx";
                            await Response.Body.WriteAsync(content);
                            Response.Body.Flush();
                        }
                        break;
                    case "csv":
                        {
                            Response.Clear();
                            Response.Headers.Add("content-disposition", "attachment;filename=users_csv.csv");
                            Response.ContentType = "application/text";
                            await Response.Body.WriteAsync(content);
                            Response.Body.Flush();
                        }
                        break;
                    case "pdf":
                        {
                            Response.Clear();
                            Response.Headers.Clear();
                            Response.ContentType = "application/pdf";
                            Response.Headers.Add("Content-Length", content.Length.ToString());
                            Response.Headers.Add("Content-Disposition", "attachment; filename=users_pdf.pdf;");
                            await Response.Body.WriteAsync(content);
                            Response.Body.Flush();
                        }
                        break;
                }

                return null;
            }
            return null;
            
        }

    }
}
