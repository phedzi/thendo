﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaleAI.BLL;
using SaleAI.Models;
using SaleAI.ViewModels;

namespace SaleAI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStoreService storeService;
        private readonly IProductService productService;
        private readonly IUserService userService;
        private string SessionUserType = "UserType";
        private string SessionUserBudget = "UserBudget";
        private string SessionUserName = "_UserName";
        private string SessionName = "_Name";
        private LoggedInUserViewModel user;
        private CultureInfo conv ;

        public HomeController(
            IStoreService storeService,
            IProductService productService,
             IUserService userService)
        {
            this.storeService = storeService;
            this.productService = productService;
            this.userService = userService;
            conv = new CultureInfo("en-US");
        }

        public async Task<IActionResult> Index()
        {
            GetCurrentUser();
            if (user.UserName == null)
                return RedirectToAction("Index", "Account");


            var From = HttpContext.Session.GetString("From");

            if(From != null && From.Equals("LogIn"))
                ViewBag.hasBuget = "No";
            else
                ViewBag.hasBuget = "Yes";

            
            HttpContext.Session.Remove("From");

            ViewBag.bread = "Home";
           var model = await storeService.GetSpecials();
            model.CurrentUser = user;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index( string name,string type,string search)
        {
            GetCurrentUser();
            var model = new HomeViewModel();

            try 
            {
                if (user.UserName == null)
                    return RedirectToAction("Index", "Account");


                if (search != null || !string.IsNullOrWhiteSpace(search))
                {
                    model = await storeService.Filter(search);
                }
                if (name != null || !string.IsNullOrWhiteSpace(name))
                {
                    if (name.ToLower().Equals("my shopping list"))
                    {
                        ViewBag.bread = conv.TextInfo.ToTitleCase("my shopping list");
                        model = await storeService.GetFavorates(user.UserName);
                    }
                    else
                    {
                        ViewBag.bread = conv.TextInfo.ToTitleCase(name);
                        model = await storeService.Filter(name);
                    }
                }
                if (type != null || !string.IsNullOrWhiteSpace(type))
                {
                    if (type.ToLower().Equals("on special"))
                    {
                        ViewBag.bread = conv.TextInfo.ToTitleCase("Home");
                        model = await storeService.GetSpecials();
                    }
                    else
                    {
                        ViewBag.bread = conv.TextInfo.ToTitleCase(type);
                        model = await storeService.GetFavorates(user.UserName);
                    }
                }

                if ((search == null || string.IsNullOrWhiteSpace(search)) && (name == null || string.IsNullOrWhiteSpace(name)) && (type == null || string.IsNullOrWhiteSpace(type)))
                {
                    ViewBag.bread = conv.TextInfo.ToTitleCase("On specials");
                    model = await storeService.GetSpecials();
                }

                model.CurrentUser = user;
            }
            catch(Exception ex)
            {

            }

            return View("Index",model);
        }


        public void GetCurrentUser()
        {
            this.user = new LoggedInUserViewModel()
            {
                UserName = HttpContext.Session.GetString(SessionUserName),
                FirstName = HttpContext.Session.GetString(SessionName),
                Budget = HttpContext.Session.GetString(SessionUserBudget),
                UserType = HttpContext.Session.GetString(SessionUserType),
            };
        }
        public async Task<JsonResult> AddProductToCategory(string storeId, bool IsImage, string Link, string Img, string Price, string Name, string user,string quan)
        {
            var product = new Product()
            {
                Store = new Store() { StoreId = System.Int32.Parse(storeId) },
                Name = Name,
                Price = Price,
                Img = Img,
                IsImage = IsImage,
                Link = Link,
                Quantity = (quan != null)? System.Int32.Parse(quan) : 0
            };
            var _newProduct = await productService.Add(product, user);
            return Json("OK");
        }

        public async Task<JsonResult> UpdateProductQuantity(string quan, string productId)
        {
            var Quantity = (quan != null) ? Int32.Parse(quan) : 0;
            var ProductId = (productId != null) ? Int32.Parse(productId) : 0;
            var _Product = productService.Get(ProductId);

            _Product.Quantity = Quantity;

            productService.Update(_Product);

            return Json("OK");
        }
        public async Task<JsonResult> AddBudgetToUser(string amount)
        {
            GetCurrentUser();
            var amt = float.Parse(amount);
            var _newProduct = await userService.AddBudget(user.UserName, amt);

            HttpContext.Session.SetString("UserBudget", amt.ToString(".00"));

            return Json("OK");
        }

        public async Task<JsonResult> GetFavorates()
        {
            var products = await productService.GetFavorates(this.User.Identity.Name);

            return Json(products);
        }

        public async Task<Product> ClearShopingList()
        {
            GetCurrentUser();
            var deleted = await productService.ClearVafourateList(user.UserName);
            return new Product();
        }

        [HttpPost]
        public async Task<IActionResult> DownloadList()
        {
            GetCurrentUser();
            var content = await productService.Download(user.UserName);

            Response.Clear();
            Response.Headers.Clear();
            Response.ContentType = "application/pdf";
            Response.Headers.Add("Content-Length", content.Length.ToString());
            Response.Headers.Add("Content-Disposition", "attachment; filename=Grocery_List.pdf;");
            await Response.Body.WriteAsync(content);
            Response.Body.Flush();

            return null;
        }

    }
}
