﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaleAI.BLL;
using SaleAI.Models;
using SaleAI.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.Controllers
{
    public class AdminController : Controller
    {
        private readonly IProductTypeService productTypeService;
        private readonly ICategoryService categoryService;
        private readonly IStoreService storeService;
        private readonly IUserService userService;
        private readonly IProductService productService;
        private string SessionUserName = "_UserName";
        private string SessionName = "_Name";
        private string SessionUserType = "UserType";
        private LoggedInUserViewModel user;
        
        public AdminController(
            IProductTypeService productTypeService,
            ICategoryService categoryService,
            IProductService productService,
            IStoreService storeService,
             IUserService userService)
        {
            this.productTypeService = productTypeService;
            this.categoryService = categoryService;
            this.storeService = storeService;
            this.productService = productService;
            this.userService = userService;
        }


        [HttpGet]
        public ActionResult Index()
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            ViewBag.img = baseurl + "/images/istockphoto-683553218-612x612.jpg";
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null)
                RedirectToAction("Login", "Account");

            if(this.user.UserType.Equals("Customer"))
                return RedirectToAction("AccessDenied", "Account");
            var dash = new PropertiesResponse();

            dash.UsersCount = userService.CountUsers();
            dash.CustomersCount = userService.CountCustomers();
            dash.AdminCount = userService.CountAdmins();
            dash.ShopsCount =  storeService.StoresCount();
            ViewBag.User = user;
            ViewBag.CustomerCount = user;
            ViewBag.User = user;
            return View(dash);
        }

        [HttpGet]
        public IActionResult StoreList()
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied","Account");
            
            ViewBag.User = user;
            return View(storeService.GetAll());
        }

        [HttpGet]
        public IActionResult Store(int? Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            
            if (Id != null && Id > 0)
            {
                ViewBag.User = user;
                return View(storeService.GetStoreView((int)Id));
            }
            else
            {
                ViewBag.User = user;
                return View(new StoreViewModel());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Store(StoreViewModel model)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            var resp = new ResponseModel();
            if (ModelState.IsValid)
            {
                if (model.Id > 0){
                    storeService.Update(model);
                    resp = new ResponseModel("ok");
                }
                else{
                    resp = await storeService.Add(model);
                }
                    
                ViewBag.Response = resp;
                ViewBag.User = user;
                return View("StoreList", storeService.GetAll());
            }
            ViewBag.Response = resp;
            ViewBag.User = user;
            return View(model);
        }

        public async Task<ActionResult> DeleteStore(int Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            var resp = new ResponseModel();
            if (Id > 0)
            {
                var store =  storeService.Get(Id);
                resp = await storeService.Delete(store);

                ViewBag.Response = resp;
                ViewBag.User = user;
                return View("StoreList", storeService.GetAll());
            }

            ViewBag.Response = resp;
            ViewBag.User = user;
            return View("StoreList", storeService.GetAll());
        }

        [HttpGet]
        public IActionResult ProductTypeList()
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            ViewBag.User = user;
            return View(productTypeService.GetAll());
        }

        [HttpGet]
        public IActionResult ProductType(int? Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            if (Id != null && Id > 0) {
                ViewBag.User = user;
                return View(productTypeService.GetProductTypeView((int)Id));
            }
            else {
                ViewBag.User = user;
                return View(new ProductTypeViewModel());
            }
        }

        [HttpPost]
        public async Task<IActionResult> ProductType(ProductTypeViewModel model)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            var resp = new ResponseModel();
            if (ModelState.IsValid)
            {
                resp = new ResponseModel("ok");
                if (model.Id > 0) 
                    productTypeService.Update(model);
                else
                    resp = await productTypeService.Add(model);

                ViewBag.Response = resp;
                ViewBag.User = user;
                return View("ProductTypeList", productTypeService.GetAll());
            }
            ViewBag.User = user;
            ViewBag.Response = resp;
            return View(model);
        }

        public async Task<ActionResult> DeleteProductType(int Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            var _resp = new ResponseModel();
            if (Id > 0)
            {
                var pt =  productTypeService.Get(Id);
                _resp = await productTypeService.Delete(pt);

                if (_resp.ErrorNo != 403) {
                    ViewBag.Response = _resp;
                    ViewBag.User = user;
                    return View("ProductTypeList", productTypeService.GetAll());
                }
                    
            }

            ViewBag.Response = _resp;
            ViewBag.User = user;
            return View("ProductTypeList", productTypeService.GetAll());
        }


        //Category 
        [HttpGet]
        public IActionResult CategoryList()
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            ViewBag.User = user;
            return View(categoryService.GetAll());
        }

        [HttpGet]
        public IActionResult Category(int? Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            if (Id != null && Id >0 ){
                ViewBag.User = user;
                return View(categoryService.GetView((int)Id));
            }
            else{
                ViewBag.User = user;
                return View(new CategoryViewModel());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Category(CategoryViewModel model)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");
            var resp = new ResponseModel();
            if (ModelState.IsValid)
            {

                if (model.Id > 0)
                {
                    categoryService.Update(model);
                    resp.Status = "Updated";
                }
                else
                    resp = await categoryService.Add(model);

                ViewBag.Response = resp;
                ViewBag.User = user;
                return View("CategoryList", categoryService.GetAll());
            }
            ViewBag.Response = resp;
            ViewBag.User = user;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteCategory(int Id)
        {
            GetCurrentUser();
            if (this.user == null || this.user.FirstName == null || this.user.UserType.Equals("Customer"))
                return View("AccessDenied", "Account");

            var _resp = new ResponseModel();
            if (Id > 0)
            {
                var category = categoryService.Get(Id);
                _resp = await categoryService.Delete(category);

                ViewBag.Response = _resp;
                ViewBag.User = user;
                return View("CategoryList", categoryService.GetAll());
            }

            ViewBag.Response = _resp;
            ViewBag.User = user;
            return View("CategoryList", categoryService.GetAll());
        }

        public void GetCurrentUser()
        {
            this.user = new LoggedInUserViewModel()
            {
                UserName = HttpContext.Session.GetString(SessionUserName),
                FirstName = HttpContext.Session.GetString(SessionName),
                UserType = HttpContext.Session.GetString(SessionUserType),
            };

        }

    }
}
