﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaleAI.BLL;
using SaleAI.ViewModels;

namespace SaleAI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService service;
        private string SessionUserName = "_UserName";
        private string SessionName = "_Name";
        private string SessionUserType =  "UserType";
        private string SessionUserBudget =  "UserBudget";
        private LoggedInUserViewModel user;
        public AccountController(IUserService service)
        {
            this.service = service;
        }
        public async Task<IActionResult> Index()
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            ViewBag.img = baseurl + "/image0.png";
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            var baseurl  = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            ViewBag.img = baseurl+ "/image0.png";
            GetCurrentUser();
            if (this.user.FirstName != null)
                return RedirectToAction("Index", "Home");

            var exist = HttpContext.Session.GetString("Exists");
             if(exist != null )
            {
                var resp = new ResponseModel();
                resp.Message = exist;
                resp.Status = "User Exists";

                ViewBag.Response = resp;
            }
             return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LogInModel login)
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            ViewBag.img = baseurl + "/image0.png";
            if (ModelState.IsValid)
            {
                var user = await service.SingInAsync(login);

                if (user != null)
                {
                    HttpContext.Session.SetString(SessionUserName, user.UserName);
                    HttpContext.Session.SetString(SessionName, user.FirstName);
                    HttpContext.Session.SetString(SessionUserType, user.Discriminator);
                    HttpContext.Session.SetString(SessionUserBudget, user.Budget.ToString(".00"));

                    if (user.Discriminator.Equals("Customer"))
                    {
                        HttpContext.Session.SetString("From", "LogIn");
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Admin");
                    }

                }
                ModelState.AddModelError(string.Empty, "Invalid Login Attemp");
            }
            return View(login);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            ViewBag.img = baseurl + "/image0.png";

            return View("Login");
        }

        [HttpGet]
        public ActionResult Register()
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";
            model.Phone = model.Phone.Trim();

            try
            {
                string api_key = "37a1e9dc10c2487295b2368024076fd3";
                string emailToValidate = model.Email;
                string ip_address = "";

                string responseString = "";

                string apiURL = "https://api.zerobounce.net/v2/validate?api_key=" + api_key + "&email=" + HttpUtility.UrlEncode(emailToValidate) + "&ip_address=" + HttpUtility.UrlEncode(ip_address);


                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Timeout = 150000;
                request.Method = "GET";

                using (WebResponse response = request.GetResponse())
                {
                    response.GetResponseStream().ReadTimeout = 20000;
                    using (StreamReader ostream = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = ostream.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                //Catch Exception - All errors will be shown here - if there are issues with the API
            }
            ViewBag.Response = new ResponseModel();
            if (ModelState.IsValid)
            {
                var _result = await service.Add(model);
                ViewBag.Response = _result;
                if (_result != null && _result.Status.ToLower()=="ok"){

                    return View("Login");
                }
                if(_result != null && _result.ErrorNo == 1)
                {
                    HttpContext.Session.SetString("Exists", _result.Message);
                    return RedirectToAction("Login", "Account");
                }
            }
            
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Profile(string username)
        {
            GetCurrentUser();
            ViewBag.user = this.user;

            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";

            if (this.user.UserName == null)
                return View("Login");
            var user = await service.ByUserName(username);

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Profile(EditProfileViewModel model)
        {
            GetCurrentUser();
            ViewBag.user = user;
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";

            if (user.UserName == null)
                return View("Login");

            if (ModelState.IsValid) {
                var result = await service.UpdateProfile(model);

                if (result != null) {
                    HttpContext.Session.Clear();
                    return View("LogIn");
                }
                else {
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult ResetPassword(string username)
        {
            GetCurrentUser();
            ViewBag.user = this.user;
            var model = new ResetPasswordViewModel()
            {
                Email = username
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            GetCurrentUser();
            ViewBag.user = this.user;

            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";
            if (ModelState.IsValid)
            {
                var results =await  service.ResetPassword(model);
                if (results != null )
                    return View("LogIn");

            }
            return View(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> Delete(string userId)
        {
            GetCurrentUser();

            var user = await service.ById(System.Int32.Parse(userId));

            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                return View("NotFound");
            }
            else
            {
                HttpContext.Session.Clear();
                var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
                ViewBag.img = baseurl + "/image0.png";

                var result = await service.Delete(user);
                if (result != null)
                    return RedirectToAction("Register");

                return View("Login");
            }
        }
        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View();
        }

        public void GetCurrentUser()
        {
            this.user = new LoggedInUserViewModel()
            {
                UserName = HttpContext.Session.GetString(SessionUserName),
                FirstName = HttpContext.Session.GetString(SessionName),
                UserType = HttpContext.Session.GetString(SessionUserType),
            };
        }


        [HttpGet]
        public ActionResult RequestResetPassword()
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RequestResetPassword(RequestReset email)
        {
            var baseurl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            ViewBag.img = baseurl + "/image0.png";
            if (ModelState.IsValid)
            {
                var results = await service.ByEmail(email.Username);
                if (results != null)
                {
                    results = await service.RequestResetPassword(results);
                    return View("LogIn");
                }
            }
            return View(email);
        }
    }
}
