﻿
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using SaleAI.DAL;
using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public class StoreService : IStoreService
    {
        private readonly IStoreRepository repository;
        private readonly ICategoryService categoryService;
        private readonly IProductTypeRepository  productTypeRepository;
        private readonly IUserRepository userRepository;
        private readonly IProductRepository productRepository;
        private readonly ILogger logger;

        public StoreService(
            IStoreRepository repository, 
            ICategoryService categoryService,
            IProductTypeRepository productTypeRepository,
            IUserRepository userRepository,
            IProductRepository productRepository,
            ILogger<StoreService> logger)
        {
            this.repository = repository;
            this.categoryService = categoryService;
            this.productTypeRepository = productTypeRepository;
            this.userRepository = userRepository;
            this.productRepository = productRepository;
            this.logger = logger;
        }

        public async Task<ResponseModel> Add(StoreViewModel model)
        {
            var resp = new ResponseModel();
            var store = new Store()
            {
                Name = model.Name,
                Link = model.Link,
                SearchLink = model.SearchLink,
                SpecialsLink = model.SpecialLink,
                Logo = model.Logo,
                Theme = model.Theme
            };
            var _store = repository.GetByName(model.Name);

            if (_store != null)
            {
                resp = new ResponseModel();
                resp.Message = "Store with the same name already exist";

                return resp;
            }

            if (await repository.Add(store) != null)
                resp = new ResponseModel("ok");

           return resp;
        }

        public Store Get(int Id)
        {
            return repository.Get(Id);
        }

        public StoreViewModel GetStoreView(int Id)
        {
            var store = Get(Id);
            var _store = new StoreViewModel()
            {
                Name = store.Name,
                Link = store.Link,
                Logo = store.Logo,
                SearchLink = store.SearchLink,
                SpecialLink = store.SpecialsLink,
                Theme = store.Theme
            };
            return _store;
        }
        public HomeViewModel GetShopStores()
        {
            var stores = new List<ShopStore>();
            var categories = new List<CategoryViewModel>();
            var productTypes = new List<ProductTypeViewModel>();
            var view = new HomeViewModel();

            try
            {
                foreach (var store in GetAll())
                {
                    stores.Add(new ShopStore(store.StoreId, store.Name, store.Link, store.SpecialsLink, store.SearchLink, store.Logo, store.Theme, new List<Product>()));
                }

                foreach (var category in categoryService.GetAll())
                {
                    categories.Add(new CategoryViewModel() { Id = category.CategoryId, Name = category.Name, IsSelected = false });
                }

                foreach (var type in productTypeRepository.GetAll())
                {
                    productTypes.Add(new ProductTypeViewModel() { Id = type.ProductTypeId, Type = type.Type });
                }

                view.stores = stores;
                view.ProductCategories = categories;
                view.ProductTypes = productTypes;
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message);
            }

            return view;
        }
        public IEnumerable<Store> GetAll()
        {
            return repository.GetAll().ToList();
        }

        public void Update(StoreViewModel model)
        {
            var store = Get(model.Id);
            store.Name = model.Name;
            store.SearchLink = model.SearchLink;
            store.SpecialsLink = model.SpecialLink;
            store.Link = model.Link;
            store.Logo = model.Logo;
            store.Theme = model.Theme;

           repository.Update(store);
        }

        public async Task<ResponseModel> Delete(Store model)
        {
            var resp = new ResponseModel("ok");
            try
            {
                var products = await productRepository.GetByStore(model.StoreId);

                if (products.Count() > 0)
                {
                    resp = new ResponseModel();
                    resp.Message = "This Store cannot be delete currently as there are \n" + products.Count().ToString() + " People who chose its products as their shoping List";
                    return resp;
                }
                await repository.Delete(model);

                resp.Status = "Deleted";
                resp.Message = "Store Successfully deleted";
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message);
            }

            return resp;
        }

        public async Task<HomeViewModel> GetSpecials()
        {
            
            var httpClient = new HttpClient();
            var htmldoc = new HtmlDocument();
            var products = new List<Product>();
            var stores = GetShopStores();

            try
            {
                foreach (var store in stores.stores)
                {
                    var html = await httpClient.GetStringAsync(store.specialLink);
                    htmldoc.LoadHtml(html);

                    switch (store.name.ToLower())
                    {
                        case "shoprite":
                            {
                                store.logo = store.link + htmldoc.DocumentNode
                                    .Descendants("div")?
                                      .Where(node => node.GetAttributeValue("class", "")
                                      .Equals("yCmsComponent header__logo"))?.FirstOrDefault()?
                                      .Descendants("img")?.FirstOrDefault()?
                                      .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                var cards = htmldoc.DocumentNode.Descendants("figure")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("item-product__content"))?.ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    var SaleProduct = new Product();

                                    SaleProduct.Name = card.Descendants("h3")?.FirstOrDefault()?.InnerText;

                                    SaleProduct.Price = card.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("special-price__price"))?.FirstOrDefault()?.InnerText;

                                    SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()
                                        .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()
                                        .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = store.link + SaleProduct.Link;
                                    store.products.Add(SaleProduct);
                                    i++;
                                    if (i >= 4) break;
                                }
                                logger.LogInformation("Shopright Collected");
                            }
                            break;
                        case "spar":
                            {
                                var checkspecial = htmldoc.DocumentNode.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("alert alert-primary no-specials"))?.FirstOrDefault()?
                                    .Descendants("h4")?.FirstOrDefault()?.InnerText;

                                var cards = htmldoc.DocumentNode.Descendants("ul")?
                                    .Where(node => node.GetAttributeValue("id", "")
                                    .Equals("slideContainer"))?.FirstOrDefault()?.Descendants("li")?.ToList();

                                store.hasSpecials = false;
                                int i = 0;
                                if (cards != null)
                                {
                                    foreach (var card in cards)
                                    {
                                        var SaleProduct = new Product();

                                        SaleProduct.Name = card.Descendants("span")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                             .Equals("valid"))?.FirstOrDefault()?.InnerText;

                                        SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()?
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                        SaleProduct.IsImage = true;
                                        store.products.Add(SaleProduct);
                                        i++;
                                        if (i >= 4) break;
                                    }
                                    store.hasSpecials = true;
                                }
                                logger.LogInformation("Spar Collected");
                            }
                            break;
                        case "checkers":
                            {
                                store.logo = store.link + GetLogo(htmldoc.DocumentNode);

                                var cards = htmldoc.DocumentNode.Descendants("figure")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("item-product__content"))?.ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    if (i >= 5)
                                    {
                                        var SaleProduct = new Product();

                                        SaleProduct.Name = card.Descendants("h3")?.FirstOrDefault()?.InnerText;

                                        SaleProduct.Price = card.Descendants("div")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("special-price__price"))?.FirstOrDefault()?
                                            .Descendants("span")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("now"))?.FirstOrDefault()?.InnerText;

                                        SaleProduct.SmImg = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()?
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                        store.products.Add(SaleProduct);
                                    }
                                    i++;
                                    if (i >= 9) break;
                                }
                                logger.LogInformation("Checkers Collected");
                            }
                            break;
                        case "pick 'n pay":
                            {
                                var cards = htmldoc.DocumentNode.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("productCarouselItem js-product-carousel-item"))?.ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    var SaleProduct = new Product();

                                    SaleProduct.Name = card.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("item-name"))?.FirstOrDefault()?.InnerText;

                                    SaleProduct.Price = card.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("currentPrice  hasSavings"))?.FirstOrDefault()?.InnerText;

                                    if (string.IsNullOrEmpty(SaleProduct.Price))
                                    {// sometimes the styles chnages for pick n pay
                                        SaleProduct.Price = card.Descendants("div")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("currentPrice  "))?.FirstOrDefault()?.InnerText;
                                    }


                                    if (SaleProduct.Price != null)
                                        SaleProduct.Price = SaleProduct.Price.Substring(0, SaleProduct.Price.Length - 2) + "." + SaleProduct.Price.Substring(SaleProduct.Price.Length - 2);

                                    SaleProduct.Img = card.Descendants("img")?.FirstOrDefault()
                                        .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()
                                        .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = store.link + SaleProduct.Link;
                                    store.products.Add(SaleProduct);
                                    i++;
                                    if (i >= 4) break;
                                }
                                logger.LogInformation("P 'N P Collected");
                            }
                            break;
                        case "game":
                            {
                                var cards = htmldoc.DocumentNode.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("carousel__item"))?.ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    var SaleProduct = new Product();

                                    SaleProduct.Name = card.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("_productnamefont"))?.FirstOrDefault()?.InnerText;

                                    SaleProduct.Price = card.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("carousel__item--price _font7"))?.FirstOrDefault()?.InnerText;


                                    if (SaleProduct.Price != null)
                                        SaleProduct.Price = SaleProduct.Price.Substring(0, SaleProduct.Price.Length - 2) + "." + SaleProduct.Price.Substring(SaleProduct.Price.Length - 2);

                                    SaleProduct.Img = card.Descendants("img")?.FirstOrDefault()
                                        .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Img = store.link + SaleProduct.Img;
                                    SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()
                                        .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = store.link + SaleProduct.Link;
                                    store.products.Add(SaleProduct);
                                    i++;
                                    if (i >= 4) break;
                                }

                                logger.LogInformation("Games Collected");
                            }
                            break;
                    }
                }
                stores.Cheapest = null;
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message);
            }

            return (HomeViewModel)stores;
        }

        public async Task<HomeViewModel> Filter(string search)
        {
            var httpClient = new HttpClient();
            var htmldoc = new HtmlDocument();
            var products = new List<Product>();
            var stores = GetShopStores();

            try
            {
                foreach (var store in stores.stores)
                {
                    if (store.searchLink != null && !string.IsNullOrWhiteSpace(store.searchLink) && store.name.ToLower() != "spar")
                    {
                        var html = await httpClient.GetStringAsync(store.searchLink + search);
                        htmldoc.LoadHtml(html);
                        switch (store.name.ToLower())
                        {
                            case "shoprite":
                                {
                                    store.logo = store.link + GetLogo(htmldoc.DocumentNode);

                                    var cards = htmldoc.DocumentNode.Descendants("figure")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("item-product__content"))?.ToList();

                                    int i = 0;
                                    foreach (var card in cards)
                                    {
                                        var _product = new Product();

                                        _product.Name = card.Descendants("h3")?.FirstOrDefault()?.InnerText;

                                        _product.Price = card.Descendants("div")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("special-price__price"))?.FirstOrDefault()?.InnerText;

                                        if (!string.IsNullOrEmpty(_product.Price)) //R1529.00
                                        {
                                            var mainPrice = _product.Price.Substring(_product.Price.IndexOf('R') + 1).Trim();
                                            var _price = mainPrice.Substring(0, mainPrice.IndexOf('.'));


                                            _product.PriceValue = (float)Convert.ToDouble(_price); //1529
                                        }

                                        _product.Img = store.link + card.Descendants("img")?.FirstOrDefault()
                                            .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                        _product.Link = store.link + card.Descendants("a")?.FirstOrDefault()
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        _product.Link = store.link + _product.Link;

                                        store.products.Add(_product);
                                        i++;
                                        if (i >= 4) break;
                                    }
                                }
                                break;
                            case "checkers":
                                {
                                    store.logo = store.link + GetLogo(htmldoc.DocumentNode);

                                    var cards = htmldoc.DocumentNode.Descendants("figure")
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("item-product__content")).ToList();

                                    int i = 0;
                                    foreach (var card in cards)
                                    {
                                        var _product = new Product();

                                        _product.Name = card.Descendants("h3")?.FirstOrDefault()?.InnerText;

                                        _product.Price = card.Descendants("div")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("special-price__price"))?.FirstOrDefault()?
                                            .Descendants("span")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("now"))?.FirstOrDefault()?.InnerText;

                                        if (!string.IsNullOrEmpty(_product.Price))
                                        {
                                            var mainPrice = _product.Price.Substring(_product.Price.IndexOf('R') + 1).Trim();
                                            var _price = mainPrice.Substring(0, mainPrice.IndexOf('.'));
                                            _product.PriceValue = (float)Convert.ToDouble(_price);
                                        }

                                        _product.SmImg = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        _product.Img = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                        _product.Link = store.link + card.Descendants("a")?.FirstOrDefault()?
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        store.products.Add(_product);
                                        i++;
                                        if (i >= 4) break;
                                    }
                                }
                                break;
                            case "pick 'n pay":
                                {
                                    var cards = htmldoc.DocumentNode.Descendants("div")
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("productCarouselItem js-product-carousel-item")).ToList();

                                    int i = 0;
                                    foreach (var card in cards)
                                    {
                                        var _product = new Product();

                                        _product.Name = card.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("item-name")).FirstOrDefault()?.InnerText;


                                        _product.Price = card.Descendants("div")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("currentPrice  hasSavings"))?.FirstOrDefault()?.InnerText;

                                        if (string.IsNullOrEmpty(_product.Price))
                                        {// sometimes the styles chnages for pick n pay
                                            _product.Price = card.Descendants("div")?
                                                .Where(node => node.GetAttributeValue("class", "")
                                                .Equals("currentPrice  "))?.FirstOrDefault()?.InnerText;
                                        }

                                        if (_product.Price != null)
                                            _product.Price = _product.Price.Substring(0, _product.Price.Length - 2) + "." + _product.Price.Substring(_product.Price.Length - 2);

                                        if (!string.IsNullOrEmpty(_product.Price))
                                        {
                                            var mainPrice = _product.Price.Substring(_product.Price.IndexOf('R') + 1).Trim();
                                            var _price = mainPrice.Substring(0, mainPrice.IndexOf('.'));
                                            _product.PriceValue = (float)Convert.ToDouble(_price);
                                        }

                                        _product.Img = card.Descendants("img")?.FirstOrDefault()
                                                    .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        if (_product.Img.Substring(0, 5) != "https")
                                            _product.Img = store.link + _product.Img;

                                        _product.Link = store.link + card.Descendants("a")?.FirstOrDefault()
                                                    .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        store.products.Add(_product);
                                        i++;
                                        if (i >= 4) break;
                                    }
                                }
                                break;
                            case "game":
                                {
                                    var cards = htmldoc.DocumentNode.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("product-item productListerGridDiv"))?.ToList();

                                    int i = 0;
                                    foreach (var card in cards)
                                    {
                                        var SaleProduct = new Product();

                                        SaleProduct.Name = card.Descendants("div")?
                                        .Where(node => node.GetAttributeValue("class", "")
                                        .Equals("product-name"))?.FirstOrDefault()?.InnerText;

                                        SaleProduct.Price = card.Descendants("span")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                            .Equals("finalPrice"))?.FirstOrDefault()?.InnerText;


                                        if (SaleProduct.Price != null)
                                            SaleProduct.Price = SaleProduct.Price.Substring(0, SaleProduct.Price.Length - 2) + "." + SaleProduct.Price.Substring(SaleProduct.Price.Length - 2);

                                        SaleProduct.Img = card.Descendants("img")?.FirstOrDefault()
                                            .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        SaleProduct.Img = store.link + SaleProduct.Img;
                                        SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                        store.products.Add(SaleProduct);
                                        i++;
                                        if (i >= 4) break;
                                    }

                                    logger.LogInformation("Games Collected");
                                }
                                break;
                        }
                    }

                    //sort products arcording to product price in accernding order
                    store.products = store.products.OrderBy(prod => prod.PriceValue).ToList();
                }

                //sort stores arcording to first product of each store price in accernding order if no product found on a specific shop
                // use the id to sort and add 999 to give it a large number so that it should not come first
                stores.stores = stores.stores.OrderBy(st => (st.products != null && st.products.Count > 0) ? st.products[0].PriceValue : (st.Id + 9999)).ToList();

                stores.Cheapest = stores.stores[0];
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message);
            }

            return (HomeViewModel)stores;
        }

        public Task<HomeViewModel> GetByCategory(string search)
        {
            throw new NotImplementedException();
        }

        public async Task<HomeViewModel> GetNormalPrices()
        {
            var httpClient = new HttpClient();
            var htmldoc = new HtmlDocument();
            var products = new List<Product>();
            var stores = GetShopStores();

            try
            {
                foreach (var store in stores.stores)
                {
                    var html = await httpClient.GetStringAsync(store.link);
                    htmldoc.LoadHtml(html);

                    switch (store.name.ToLower())
                    {
                        case "shoprite":
                            {
                                store.logo = store.link + htmldoc.DocumentNode
                                        .Descendants("div")?
                                       .Where(node => node.GetAttributeValue("class", "")
                                       .Equals("yCmsComponent header__logo"))?.FirstOrDefault()?
                                       .Descendants("img")?.FirstOrDefault()?
                                       .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                var cards = htmldoc.DocumentNode.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("carousel__item"))?.ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    var SaleProduct = new Product();

                                    SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()
                                        .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()
                                        .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                    var hasWW = SaleProduct.Link.Substring(0, 8);
                                    if (hasWW.Trim().ToLower() != "https://")
                                    {
                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                    }


                                    store.products.Add(SaleProduct);
                                    i++;
                                    if (i >= 4) break;
                                }
                            }
                            break;
                        case "spar":
                            {
                                var checkspecial = htmldoc.DocumentNode.Descendants("div")?
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("alert alert-primary no-specials"))?.FirstOrDefault()?
                                    .Descendants("h4")?.FirstOrDefault()?.InnerText;

                                var cards = htmldoc.DocumentNode.Descendants("ul")?
                                    .Where(node => node.GetAttributeValue("id", "")
                                    .Equals("slideContainer"))?.FirstOrDefault()?.Descendants("li")?.ToList();

                                store.hasSpecials = false;
                                int i = 0;
                                if (cards != null)
                                {
                                    foreach (var card in cards)
                                    {
                                        var SaleProduct = new Product();

                                        SaleProduct.Name = card.Descendants("span")?
                                            .Where(node => node.GetAttributeValue("class", "")
                                             .Equals("valid"))?.FirstOrDefault()?.InnerText;

                                        SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()?
                                            .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()?
                                            .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                        SaleProduct.IsImage = true;
                                        store.products.Add(SaleProduct);
                                        i++;
                                        if (i >= 4) break;
                                    }
                                    store.hasSpecials = true;
                                }
                            }
                            break;
                        case "checkers":
                            {
                                store.logo = store.link + GetLogo(htmldoc.DocumentNode);

                                var cards = htmldoc.DocumentNode.Descendants("div")
                                    .Where(node => node.GetAttributeValue("class", "")
                                    .Equals("carousel__item")).ToList();

                                int i = 0;
                                foreach (var card in cards)
                                {
                                    var SaleProduct = new Product();

                                    SaleProduct.SmImg = store.link + card.Descendants("img")?.FirstOrDefault()?
                                        .ChildAttributes("src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Img = store.link + card.Descendants("img")?.FirstOrDefault()?
                                        .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;

                                    SaleProduct.Link = card.Descendants("a")?.FirstOrDefault()?
                                        .ChildAttributes("href")?.FirstOrDefault()?.Value;

                                    var hasWW = SaleProduct.Link.Substring(0, 8);
                                    if (hasWW.Trim().ToLower() != "https://")
                                    {
                                        SaleProduct.Link = store.link + SaleProduct.Link;
                                    }

                                    store.products.Add(SaleProduct);
                                    i++;
                                    if (i >= 4) break;
                                }
                            }
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message);
            }

            return (HomeViewModel)stores;
        }

        public async Task<HomeViewModel> GetFavorates(string UserName)
        {

            var quantity = 0;
            var amount = (float)0;
            var stores = new List<ShopStore>();
            var priceVal = "";
            try
            {
                var user = await userRepository.Get(UserName);
                var _user = await userRepository.GetUserWithProducts(user.UserId);

                foreach (var prod in _user.UserProducts)
                {

                    var product = prod.Product;
                    var amt = (!prod.Product.IsImage) ? prod.Product.Price.Substring(prod.Product.Price.LastIndexOf("R") + 1) : "0";

                    var priceArr = amt.Split('.');
                     priceVal = priceArr[0] + "." + priceArr[priceArr.Length - 1];

                    amount = amount + (float.Parse(amt) * prod.Product.Quantity);
                    quantity = quantity + prod.Product.Quantity;
                    product.Category = new Category() { Name = "Favorate", Selected = true };
                    product.Selected = "in-list";
                    var prods = new List<Product>();
                    prods.Add(product);

                    var store = product.Store.Name.Trim().ToLower();

                    if (store == "spar" || store == "pick 'n pay")
                    {
                        stores.Add(new ShopStore()
                        {
                            Id = product.Store.StoreId,
                            name = product.Store.Name,
                            link = product.Store.Link,
                            specialLink = product.Store.SpecialsLink,
                            searchLink = product.Store.SearchLink,
                            logo = product.Store.Logo,
                            theme = product.Store.Theme,
                            products = prods
                        });
                    }
                    else
                    {
                        stores.Add(new ShopStore()
                        {
                            Id = product.Store.StoreId,
                            name = product.Store.Name,
                            link = product.Store.Link,
                            specialLink = product.Store.SpecialsLink,
                            searchLink = product.Store.SearchLink,
                            logo = product.Store.Logo,
                            theme = product.Store.Theme,
                            products = prods
                        });
                    }

                }
                logger.LogInformation(" The priceval is " + priceVal);
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message + "string is " + priceVal);
            }

            var _homeModel = GetShopStores();
            _homeModel.stores = stores;
            _homeModel.totalQuantity = quantity;
            _homeModel.totalAmount = amount;

            return _homeModel;
        }


        //PRIVATE METHODS
        private string GetLogo(HtmlNode node)
        {
            var logo = node.Descendants("div")?
                                  .Where(node => node.GetAttributeValue("class", "")
                                  .Equals("yCmsComponent header__logo"))?.FirstOrDefault()?
                                  .Descendants("img")?.FirstOrDefault()?
                                  .ChildAttributes("data-original-src")?.FirstOrDefault()?.Value;
            return (logo == null) ? "":logo ;
        }

        public int StoresCount()
        {
            return GetAll().Count();
        }
    }
}
