﻿using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public interface IProductTypeService
    {
        ProductType Get(int Id);
        ProductTypeViewModel GetProductTypeView(int Id);
        IEnumerable<ProductType> GetAll();
        Task<ResponseModel> Add(ProductTypeViewModel model);
        void Update(ProductTypeViewModel model);
        Task<ResponseModel> Delete(ProductType model);
    }
}
