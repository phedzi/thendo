﻿using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public interface IProductService
    {
        Product Get(int Id);
        Task<bool> IsProductDeleted(int prodId);
        Task<List<Product>> GetProductByStore(int StoreId);
        IEnumerable<Product> GetAll();
        Task<IEnumerable<Product>> GetFavorates(string username);
        Task<Product> Add(Product model, string user);
        Product Update(Product model);
        Task<Product> Delete(Product model);

        Task<byte[]> Download(string username);
        Task<bool> ClearVafourateList(string username);
    }
}
