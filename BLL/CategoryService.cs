﻿using SaleAI.DAL;
using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository repository;

        public CategoryService(ICategoryRepository repository)
        {
            this.repository = repository;
        }
        public async Task<ResponseModel> Add(CategoryViewModel model)
        {
            var category = new Category() {   Name = model.Name,  Selected = false , Deletable = true};
            var resp = new ResponseModel();
            var _category = GetByName(model.Name.ToLower());

            if(_category != null)
            {
                resp.Message = "Actegory already exists";
                return resp;
            }
            await repository.Add(category);

            return new ResponseModel("ok");
        }
        public Category Get(int Id)
        {
            return repository.Get(Id);
        }
        public Category GetByName(string Name)
        {
            return repository.GetByName(Name);
        }
        public CategoryViewModel GetView(int Id)
        {
            var category = Get(Id);
            var _category = new CategoryViewModel()
            {
                Id = category.CategoryId,
                Name = category.Name
            };

            return _category;
        }
        public IEnumerable<Category> GetAll()
        {
            return repository.GetAll().ToList();
        }
        public void Update(CategoryViewModel model)
        {
            var category = new Category();
            category = Get(model.Id);
            category.Name = model.Name;

            repository.Update(category);
        }
        public async Task<ResponseModel> Delete(Category model)
        {
            await repository.Delete(model);
            var resp = new ResponseModel("ok");

            resp.Status = "Deleted";
            resp.Message = "Category Successfully deleted";
            return resp;
        }
    }
}
