﻿using SaleAI.Models;
using SaleAI.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using SaleAI.ViewModels;
using AIStore.DAL;
using AIStore.BLL;
using System.Data;
using System.Globalization;
using Microsoft.Extensions.Logging;

namespace SaleAI.BLL
{
    public class UserService : IUserService
    {
        private readonly IUserRepository repository;
        private readonly IUserLogInRepository userLogInRepository;
        private readonly IExportService exportService;
        private readonly ILogger logger;
        public UserService( IUserRepository repository,
            IUserLogInRepository userLogInRepository,
            IExportService exportService,
            ILogger<UserService> logger)
        {
            this.repository = repository;
            this.userLogInRepository = userLogInRepository;
            this.exportService = exportService;
            this.logger = logger;
        }
        public async Task<ResponseModel> Add(RegisterModel model)
        {
            var user = new User();
            var resp = new ResponseModel();
            user.FirstName = model.Name;
            user.UserName = model.Email.Trim();
            user.PhoneNumber = model.Phone;

            var exists = await repository.ByUserName(user.UserName);
            if (exists == null)
            {
                
                if(model.Password.Length< 8)
                {
                    resp.Message = "The Password Must be at least 8 charectors long";
                    return resp;
                }
                else if (!model.Password.Any(char.IsDigit))
                {
                    resp.Message = "The Password Must be at least contain 1 digit";
                    return resp;
                }
                else if(!model.Password.Any(char.IsLower))
                {
                    resp.Message = "The Password Must be at least contain 1 lower alpha";
                    return resp;
                }
                else if(!model.Password.Any(char.IsUpper))
                {
                    resp.Message = "The Password Must be at least contain 1 capital letter";
                    return resp;
                }
                else if(!hasCharector(model.Password))
                {
                    resp.Message = "The Password Must be at least contain 1 of the following special charectors \n ! @ # $ % ^ & * ? | + ";
                    return resp;
                }

                var newUser = new User();

                if (repository.GetAll().ToList().Count < 2)
                {
                    newUser = new SuperAdmin()
                    {
                        FirstName = user.FirstName,
                        PhoneNumber = user.PhoneNumber,
                        UserName = user.UserName
                    };
                }
                else
                {
                    newUser = new Customer()
                    {
                        FirstName = user.FirstName,
                        PhoneNumber = user.PhoneNumber,
                        UserName = user.UserName
                    };
                }

                var result = await repository.Add(newUser, model.Password);
                if (result != null)
                {
                    resp = new ResponseModel("ok");
                    resp.Message = "Successfully Registered";
                }
            }
            else
            {
                resp.Message = "User account :" + exists.UserName + " already exists try loging in";
                resp.ErrorNo = 1;
            }
            
            return resp;
        }
        public List<User> GetAll()
        {
            return repository.GetAll().ToList();
        }

        public List<ReportUserViewModel> GetReport(string filter)
        {
            var users = new List<User>();
            var _users = new List<ReportUserViewModel>();
            var convert = new CultureInfo("en-US");

            try
            {
                if (filter == null || filter.Equals("All"))
                {
                    users = GetAll();
                }
                else
                {
                    users = repository.Filter(filter).ToList();
                }

                foreach (var user in users)
                {
                    _users.Add(new ReportUserViewModel()
                    {
                        UserId = user.UserId,
                        FullName = convert.TextInfo.ToTitleCase(user.FirstName),
                        PhoneNumber = user.PhoneNumber,
                        UserName = user.UserName.ToLower(),
                        UserLogInCount = (user.UserLogIns != null) ? user.UserLogIns.Count : 0,
                        RequestedPasswordReset = user.RequestedPasswordReset,
                        UserTypeName = user.Discriminator,
                        CreatedAt = user.CreatedAt.ToString("dd MM yyy")
                    });
                }
            }
            catch(Exception ex)
            {
                logger.LogError(ex.Message);
            }

            return _users;
        }
        public async Task<User> ById(int Id)
        {
            return await repository.GetById(Id);
        }
        public async Task<UserDetailViewModel> UserDetails(User user)
        {
            var _user = new UserViewModel()
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                PhoneNumber = user.PhoneNumber,
                Type = user.Discriminator,
                RequestedPasswordReset = user.RequestedPasswordReset,
                UserName = user.UserName
            };
            var model = new UserDetailViewModel()
            {
                User = _user
            };
            return model;
        }
        public async Task<User> ByEmail(string email)
        {
            return await repository.ByUserName(email);
        }
        public async Task<EditProfileViewModel> ByUserName(string username)
        {
            var user = await repository.ByUserName(username.Trim());
            if (user == null)
                return new EditProfileViewModel() { UserFound = false, Message = username + "does not Exist" };

            var model = new EditProfileViewModel()
            {
                Id = user.UserId,
                FirstName = user.FirstName,
                Email = user.UserName,
                Phone = user.PhoneNumber,
                CreatedDate = user.CreatedAt.Date.ToString(),
                UserFound = true
            };

            return model;
        }
        public async Task<User> UpdateProfile(EditProfileViewModel User)
        {
            var user = await ById(User.Id);

            user.FirstName = User.FirstName;
            user.UserName = User.Email;
            user.PhoneNumber = User.Phone;

            return await repository.Update(user);
        }
        public async Task<User> Delete(User model)
        {
            var user = await repository.GetUserWithProducts(model.UserId);

            user.IsDeleted = true;
             var _user = await repository.Update(user);

            return _user;
        }
        public async Task<User> SingInAsync(LogInModel user)
        {
            var _user = await repository.Get(user.UserName);

            if (_user == null)
                return null;

            var log = new UserLogIn();
            if (await repository.LogIn(user)){

                _user.IsDeleted = false ;
                await repository.Update(_user);

                log.User = _user;
                await userLogInRepository.Add(log);
                var userWith = await repository.GetById(_user.UserId);
                return userWith;
            }

            return null;
        }
        public async Task<User> ResetPassword(ResetPasswordViewModel model)
        {
            var user = await repository.ByUserName(model.Email.Trim());

            return await repository.ResetPassword(user, model.Password);
        }
        public async Task<User> RequestResetPassword(User model)
        {
            model.RequestedPasswordReset = true;

            await repository.Update(model);

            return model;
        }
        private bool hasCharector(string specialcharectors)
        {
            char[] chars = new char[] { '!', '@', '#', '$', '%', '^', '&', '*', '?', '|', '+' };
            var response = false;

            foreach(char val in chars)
            {
                var exist = specialcharectors.Where(c => c.ToString().Equals(val.ToString())).FirstOrDefault();
                response = ((int)exist > 0) ? true:false ;

                if (response) return response;
            }
            return response;
        }

        public async Task<byte[]> ExportData(string expType,string filter)
        {
            var users = new List<User>();
            var _users = new List<ExportUser>();
            var convert = new CultureInfo("en-US");

            try
            {
                if (filter == null || filter.Equals("All"))
                {
                    users = GetAll();
                }
                else
                {
                    users = repository.Filter(filter).ToList();
                }

                foreach (var user in users)
                {
                    _users.Add(new ExportUser()
                    {
                        UserId = user.UserId,
                        FullName = convert.TextInfo.ToTitleCase(user.FirstName),
                        PhoneNumber = user.PhoneNumber,
                        UserName = user.UserName.ToLower(),
                        UserLogInCount = user.UserLogIns.Count,
                        UserTypeName = user.Discriminator,
                        CreatedAt = user.CreatedAt.ToString("dd MM yyy")
                    });
                }

            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }

            var results = exportService.ExportToExcel(_users);
            switch (expType)
            {
                case "xlsx":
                    results = exportService.ExportToExcel(_users);
                    break;
                case "csv":
                    results = exportService.ExportToCsv(_users);
                    break;
                case "pdf":
                    results = await exportService.ExportToPdf(_users);
                    break;
                case "Json":
                    results = exportService.ExportToJson(_users);
                    break;
            }

            return results; 
        }

        public async Task<User> ChangeUserType(int Id,string type)
        {
            var user = await ById(Id);

            user.Discriminator = type;
            return await repository.Update(user);
        }

        public async Task<User> AddBudget(string userName, float amt)
        {
            var user = await ByEmail(userName);
            user.Budget = amt;

            return await repository.Update(user);
            return await repository.Update(user);
        }

        public int CountUsers()
        {
            return GetAll().Count();
        }
        public int CountCustomers()
        {
            var customers = repository.Filter("Customer");

            return customers.Count();
        }
        public int CountAdmins()
        {
            var admis = repository.Filter("Administrator");
            var count = admis.Count();

            var supers = repository.Filter("SuperAdmin");

            var sum = count + supers.Count();

            return sum;
        }
    }
}
