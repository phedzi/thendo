﻿using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public interface ICategoryService
    {
        Category Get(int Id);
        CategoryViewModel GetView(int Id);
        Category GetByName(string Name);
        IEnumerable<Category> GetAll();
        Task<ResponseModel> Add(CategoryViewModel model);
        void Update(CategoryViewModel model);
        Task<ResponseModel> Delete(Category model);
        
    }
}
