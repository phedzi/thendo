﻿using SaleAI.DAL;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SaleAI.ViewModels;
using AIStore.ViewModels;

namespace SaleAI.BLL
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository repository;
        private readonly IProductTypeRepository productTypeRepository;
        private readonly IStoreRepository storeRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IUserProductRepository userProductRepository;
        private readonly IUserRepository userRepository;

        public ProductService(IProductRepository repository, 
            IProductTypeRepository productTypeRepository,
           IStoreRepository storeRepository, 
           ICategoryRepository categoryRepository,
           IUserProductRepository userProductRepository,
           IUserRepository userRepository,
           IStoreService storeService)
        {
            this.repository = repository;
            this.productTypeRepository = productTypeRepository;
            this.categoryRepository = categoryRepository;
            this.storeRepository = storeRepository;
            this.userRepository = userRepository;
            this.userProductRepository = userProductRepository;
        }

        public async Task<Product> Add(Product model,string username)
        {
           
            if (model.Store != null)
                model.Store = storeRepository.Get(model.Store.StoreId);

            model.Type = productTypeRepository.GetByName("On Special");
            model.Category = categoryRepository.GetByName("favorate");


            var user = await userRepository.Get(username);
            if (user == null)
                return null;

            var product = repository.GetByProduct(model);

            // if not null means this product on this store already exist
            if(product == null)
                product = await repository.Add(model);
            
            
            
            // after creating the user add it to userproduct
            var userProd = new UserProduct()
            {
                User = user,
                UserId = user.UserId,
                Product = product,
                ProductId = product.ProductId
            };


            if (!repository.AssignedToUser(userProd))
                await userProductRepository.Add(userProd);
            else
                Delete(product);
            return product;
        }

        public Product Get(int Id)
        {
            return repository.Get(Id);
        }

        public IEnumerable<Product> GetAll()
        {
            return repository.GetAll();
        }

        public Product Update(Product model)
        {
            return repository.Update(model);
        }

        public async Task<Product> Delete(Product model)
        {
            await repository.Delete(model);
            return model;
        }

        public async Task<IEnumerable<Product>> GetFavorates(string username)
        {
            var user = await userRepository.Get(username);

            return await repository.GetAllByUser(user); ;
        }

        public async Task<List<Product>> GetProductByStore(int StoreId)
        {
            return await repository.GetByStore(StoreId);
        }

        public async Task<bool> IsProductDeleted(int prodId)
        {
            return await userProductRepository.IsProductDeleted(prodId);
        }

        public async Task<bool> ClearVafourateList(string username)
        {
            var user = await userRepository.Get(username);

             var products = await repository.GetAllByUser(user);
            var meesage= "";
            if (products.Any())
            {
                foreach(var product in products)
                {
                    try
                    {
                        await repository.Delete(product);
                    }
                    catch (Exception ex)
                    {
                        meesage = ex.Message;
                    }
                   
                }
            }

            return true;
        }

        public async Task<byte[]> Download(string username)
        {
            var user = await userRepository.Get(username);
            var products = await repository.GetAllByUser(user);
            var TransformedProducts = Transform(products.ToList());
            var _users = ConvertToDataTable(TransformedProducts);
            var imagesDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files");


            float marginLeft = 72;
            float marginTop = 60;

            if (!Directory.Exists(imagesDirectory))
                Directory.CreateDirectory(imagesDirectory);

            var filename = $"_{Guid.NewGuid()}.pdf";
            var filePath = Path.Combine(imagesDirectory, filename);
            var response = new byte[0];

            try
            {
                if (_users.Rows.Count > 0)
                {
                    List<String> columns = new List<string>();

                    Document document = new Document(PageSize.A4, 5f, 5f, 10f, 10f);
                    FileStream fs = new FileStream(filePath, FileMode.Create);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    document.Open();

                    document.AddTitle("GROCERY LIST OF :" + username.ToUpper());
                    document.AddAuthor(user.FirstName);
                    Font font1 = FontFactory.GetFont(FontFactory.COURIER_BOLD, 10);
                    Font fontHeader = FontFactory.GetFont("Calibri", 30, Font.BOLD, new BaseColor(0, 0, 0));
                    Font font2 = FontFactory.GetFont(FontFactory.COURIER, 8);


                    // Create header
                    PdfContentByte cb = writer.DirectContent;
                    cb.MoveTo(marginLeft, marginTop);
                    cb.LineTo(500, marginTop);
                    cb.Stroke();

                    Paragraph paraHeader_1 = new Paragraph("List Budget Is : R" + user.Budget + ".00", fontHeader);

                    paraHeader_1.Alignment = Element.ALIGN_CENTER;
                    paraHeader_1.SpacingAfter = 2f;
                    document.Add(paraHeader_1);
                    float[] columnDefinitionSize = { 7F, 2F, 2F, 3F, };
                    PdfPTable table;
                    PdfPCell cell;

                    table = new PdfPTable(columnDefinitionSize)
                    {
                        WidthPercentage = 100
                    };

                    cell = new PdfPCell
                    {
                        BackgroundColor = new BaseColor(0xC0, 0xC0, 0xC0)
                    };

                    foreach (DataColumn column in _users.Columns)
                    {
                        columns.Add(column.ColumnName);
                        table.AddCell(new Phrase(column.ColumnName, font1));
                    }

                    table.HeaderRows = 1;
                    foreach (DataRow data in _users.Rows)
                    {
                        foreach (String col in columns)
                        {
                            table.AddCell(new Phrase(data[col].ToString(), font2));
                        }
                    }

                    table.SpacingAfter = 10f;

                    Paragraph linez = new Paragraph(". ", font2);
                    linez.Alignment = Element.ALIGN_BOTTOM;
                    document.Add(linez);

                    document.Add(table);


                    // get Products quantity count and shop counts and total amount count
                    var totalQuantity = 0;
                    var totalAmount = 0.0;
                    var captionAgainstBudget = "Total amount is Within budget";

                    foreach (var prod in products)
                    {
                        totalQuantity += prod.Quantity;
                        var price = prod.Price.Substring(prod.Price.LastIndexOf("R") + 1);
                        var priceArr = price.Split('.');
                        var priceVal = float.Parse(priceArr[0]+"."+priceArr[priceArr.Length-1]);

                        totalAmount = totalAmount + (priceVal * prod.Quantity);
                    }

                    if (totalAmount > user.Budget)
                        captionAgainstBudget = "Total amount is Above Budget ";


                    Paragraph footer1 = new Paragraph("Total Products In List : " + totalQuantity, font1);
                    footer1.Alignment = Element.ALIGN_BOTTOM;
                    document.Add(footer1);

                    Paragraph footer2 = new Paragraph("Total Amount Price : R " + totalAmount.ToString(".00"), font1);
                    footer2.Alignment = Element.ALIGN_BOTTOM;
                    document.Add(footer2);

                    font1.Color = BaseColor.BLUE;
                    Paragraph footer3 = new Paragraph("Message :" + captionAgainstBudget.ToUpper(), font1);
                    footer3.Alignment = Element.ALIGN_BOTTOM;
                    document.Add(footer3);

                    document.Close();
                    document.CloseDocument();
                    document.Dispose();
                    writer.Close();
                    writer.Dispose();
                    fs.Close();
                    fs.Dispose();

                    FileStream sourceFile = new FileStream(filePath, FileMode.Open);
                    float fileSize = 0;
                    fileSize = sourceFile.Length;
                    response = new byte[Convert.ToInt32(Math.Truncate(fileSize))];

                    await sourceFile.ReadAsync(response, 0, Convert.ToInt32(sourceFile.Length));
                    sourceFile.Close();
                }
            }
            catch(Exception ex)
            {
                var b = ex.Message;
                var sdd = b;
            }
         
              
            return response;
        }


        private DataTable ConvertToDataTable(List<ExportProduct> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(ExportProduct));

            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (ExportProduct item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        private List<ExportProduct> Transform(List<Product> products)
        {
            var _products = new List<ExportProduct>();

            foreach (var product in products)
            {
                _products.Add(new ExportProduct()
                {
                    Product = product.Name,
                    Quantity = product.Quantity,
                    Price = product.Price,
                    Shop = product.Store.Name
                });
            }

            return _products;
        }
    }
}
