﻿using SaleAI.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AIStore.BLL
{
    public interface IExportService
    {
        byte[] ExportToExcel(List<ExportUser> users);
        byte[] ExportToCsv(List<ExportUser> users);
        Task<byte[]> ExportToPdf(List<ExportUser> users);
        byte[] ExportToJson(List<ExportUser> users);

    }
}
