﻿using SaleAI.Models;
using SaleAI.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public interface IStoreService
    {
        Store Get(int Id);
        StoreViewModel GetStoreView(int Id);
        Task<HomeViewModel> GetSpecials();
        Task<HomeViewModel> GetFavorates(string userName);
        Task<HomeViewModel> Filter(string search);
        IEnumerable<Store> GetAll();
        HomeViewModel GetShopStores();
        Task<ResponseModel> Add(StoreViewModel model);
        void Update(StoreViewModel model);
        Task<ResponseModel> Delete(Store model);
        Task<HomeViewModel> GetByCategory(string search);
        Task<HomeViewModel> GetNormalPrices();
        int StoresCount();
    }
}
