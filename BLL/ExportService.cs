﻿using System.Data;
using ClosedXML.Excel;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using SaleAI.ViewModels;
using Microsoft.Extensions.Logging;

namespace AIStore.BLL
{
    public class ExportService : IExportService
    {
        private readonly ILogger logger;
        public ExportService(ILogger<ExportService> logger)
        {
            this.logger = logger;
        }

        public byte[] ExportToCsv(List<ExportUser> users)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var _users = ConvertToDataTable(users);
                List<String> columns = new List<string>();

                foreach (DataColumn column in _users.Columns)
                {
                    columns.Add(column.ColumnName);
                }

                sb.AppendLine(string.Join(",", columns));
                foreach (DataRow row in _users.Rows)
                {
                    IEnumerable<string> fields = row.ItemArray.Select(field =>
                      string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                    sb.AppendLine(string.Join(",", fields));
                }
            }
            catch(Exception ex)
            {
                logger.LogError(ex.Message);
            }

            return ASCIIEncoding.ASCII.GetBytes(sb.ToString());
        }

        public byte[] ExportToExcel(List<ExportUser> users)
        {
            var _users = ConvertToDataTable(users);
            using var stream = new MemoryStream();
            try
            {
                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("Users");
                    var currentRow = 1;
                    List<String> columns = new List<string>();
                    int columnIndex = 1;

                    foreach (DataColumn column in _users.Columns)
                    {
                        columns.Add(column.ColumnName);
                        worksheet.Cell(currentRow, columnIndex).Value = column.ColumnName;
                        worksheet.Cell(currentRow, columnIndex).Style.Font.Bold = true;
                        columnIndex++;
                    }

                    foreach (DataRow user in _users.Rows)
                    {
                        currentRow++;
                        int cellIndex = 1;
                        foreach (String col in columns)
                        {
                            worksheet.Cell(currentRow, cellIndex).Value = user[col].ToString();
                            cellIndex++;
                        }
                    }

                    workbook.SaveAs(stream);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }

            return stream.ToArray();
        }

        public byte[] ExportToJson(List<ExportUser> users)
        {
            var _users = ConvertToDataTable(users);
            var listProduct = (from DataRow row in _users.Rows
                               select new ExportUser()
                               {
                                   UserId = row["UserId"] != null ? Convert.ToInt32(row["UserId"]) : 0,
                                   UserName = Convert.ToString(row["UserName"]),
                                   FullName = Convert.ToString(row["FullName"]),
                                   PhoneNumber = Convert.ToString(row["PhoneNumber"]),
                                   UserLogInCount = row["UserLogInCount"] != null ? Convert.ToInt32(row["UserLogInCount"]) : 0,
                                   UserTypeName = Convert.ToString(row["UserTypeName"]),
                                   CreatedAt = Convert.ToString(row["CreatedAt"])
                               }).ToList();

           string jsonProductList = new JavaScriptSerializer().Serialize(listProduct);

           return  ASCIIEncoding.ASCII.GetBytes(jsonProductList);

        }

        public async Task<byte[]> ExportToPdf(List<ExportUser> users)
        {
            var _users = ConvertToDataTable(users);
            var imagesDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files");

            if (!Directory.Exists(imagesDirectory))
                Directory.CreateDirectory(imagesDirectory);

            var filename = $"_{Guid.NewGuid()}.pdf";
            var filePath = Path.Combine(imagesDirectory, filename);
            var response = new byte[0];

            try 
            {
                if (_users.Rows.Count > 0)
                {
                    List<String> columns = new List<string>();

                    iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4, 5f, 5f, 10f, 10f);
                    FileStream fs = new FileStream(filePath, FileMode.Create);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    document.Open();

                    Font font1 = FontFactory.GetFont(FontFactory.COURIER_BOLD, 10);
                    Font font2 = FontFactory.GetFont(FontFactory.COURIER, 8);

                    float[] columnDefinitionSize = { 1F, 2F, 2F, 2F, 2F, 2F, 2F };
                    PdfPTable table;
                    PdfPCell cell;

                    table = new PdfPTable(columnDefinitionSize)
                    {
                        WidthPercentage = 100
                    };

                    cell = new PdfPCell
                    {
                        BackgroundColor = new BaseColor(0xC0, 0xC0, 0xC0)
                    };

                    foreach (DataColumn column in _users.Columns)
                    {
                        columns.Add(column.ColumnName);
                        table.AddCell(new Phrase(column.ColumnName, font1));
                    }


                    table.HeaderRows = 1;
                    foreach (DataRow data in _users.Rows)
                    {
                        foreach (String col in columns)
                        {
                            table.AddCell(new Phrase(data[col].ToString(), font2));
                        }
                    }

                    document.Add(table);
                    document.Close();
                    document.CloseDocument();
                    document.Dispose();
                    writer.Close();
                    writer.Dispose();
                    fs.Close();
                    fs.Dispose();

                    FileStream sourceFile = new FileStream(filePath, FileMode.Open);
                    float fileSize = 0;
                    fileSize = sourceFile.Length;
                    response = new byte[Convert.ToInt32(Math.Truncate(fileSize))];

                    await sourceFile.ReadAsync(response, 0, Convert.ToInt32(sourceFile.Length));
                    sourceFile.Close();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
          
            return response;
        }
        private DataTable ConvertToDataTable(List<ExportUser> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(ExportUser));

            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (ExportUser item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
