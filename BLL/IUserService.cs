﻿using AIStore.ViewModels;
using SaleAI.Models;
using SaleAI.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public interface IUserService
    {
        List<User> GetAll();
        List<ReportUserViewModel> GetReport(string filter);
        Task<User> ById(int Id);
        Task<User> ByEmail(string email);
        Task<UserDetailViewModel> UserDetails(User user);
        Task<EditProfileViewModel> ByUserName(string username);
        Task<ResponseModel> Add(RegisterModel model);
        Task<User> UpdateProfile(EditProfileViewModel User);
        Task<User> ResetPassword(ResetPasswordViewModel model);
        Task<User> RequestResetPassword(User model);
        Task<User> Delete(User model);
        Task<byte[]> ExportData(string expType, string filter);
        Task<User> SingInAsync(LogInModel user);
        Task<User> ChangeUserType(int Id, string type);
        Task<User> AddBudget(string userName, float amt);

        int CountUsers();
        int CountCustomers();
        int CountAdmins();
    }
}
