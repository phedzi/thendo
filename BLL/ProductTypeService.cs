﻿using SaleAI.DAL;
using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.BLL
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly IProductTypeRepository repository;

        public ProductTypeService(IProductTypeRepository repository)
        {
            this.repository = repository;
        }
        public async Task<ResponseModel> Add(ProductTypeViewModel model)
        {
            var type = new ProductType() { Type = model.Type, Deletable = true };

            var resp = new ResponseModel();
            if(await repository.Add(type) == null)
            {
                resp = new ResponseModel("ok");
            }

            return resp;
        }
        public ProductType Get(int Id)
        {
            return  repository.Get(Id);
        }
        public ProductTypeViewModel GetProductTypeView(int Id)
        {
            var type = repository.Get(Id);
            var _type = new ProductTypeViewModel()
            {
                Id = type.ProductTypeId,
                Type = type.Type
            };

            return _type;
        }
        public IEnumerable<ProductType> GetAll()
        {
            return repository.GetAll().ToList();
        }
        public void Update(ProductTypeViewModel model)
        {
            var type = Get(model.Id);
            type.Type = model.Type;

            repository.Update(type);
        }

        public async Task<ResponseModel> Delete(ProductType model)
        {
            await repository.Delete(model);

            var resp = new ResponseModel("ok");

            resp.Status = "Deleted";
            resp.Message = "Category Successfully deleted";
            return resp;
        }
    }
}
