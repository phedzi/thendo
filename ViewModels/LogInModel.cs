﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.ViewModels
{
    public class LogInModel
    {
        //we changed username to email to avoid duplicated usernames
        [Required]
        [EmailAddress]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display (Name ="Remember Me")]
        public bool RememberMe { get; set; }
    }
}
