﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Full Name is Required")]
        [StringLength(30, MinimumLength = 4)]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Input valid Phone Number")]
        [StringLength(20, MinimumLength = 10)]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public string ErrorMessage {
            get { 
                return ""; 
            } 
            set {
                if (Password == "") ;
            } 
        }
    }
}
