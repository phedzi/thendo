﻿using SaleAI.Models;

namespace SaleAI.ViewModels
{
    public class ProductLikeViewModel
    {
        public ShopStore Store { get; set; }
        public Product Product { get; set; }
    }
}
