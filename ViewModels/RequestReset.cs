﻿
using System.ComponentModel.DataAnnotations;

namespace SaleAI.ViewModels
{
    public class RequestReset
    {

        [Required(ErrorMessage = "Email is Required as your UserName")]
        [StringLength(50, MinimumLength = 6)]
        [EmailAddress]
        [Display(Name = "User's Email Address")]
        public string Username { get; set; }
    }
}
