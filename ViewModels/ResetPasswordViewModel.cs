﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.ViewModels
{
    public class ResetPasswordViewModel
    {

        [Required(ErrorMessage = "Email is Required as your UserName")]
        [StringLength(50, MinimumLength = 6)]
        [EmailAddress]
        [Display(Name = "User's Email Address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
