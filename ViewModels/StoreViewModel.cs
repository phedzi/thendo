﻿using System.ComponentModel.DataAnnotations;

namespace SaleAI.ViewModels
{
    public class StoreViewModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Store Website Address")]
        [StringLength(75, MinimumLength = 2)]
        public string Link { get; set; }

        [Required]
        [Display(Name = "Logo Link")]
        [StringLength(150, MinimumLength = 2)]
        public string Logo { get; set; }

        [Required]
        [Display(Name = "Specials Link")]
        [Url]
        [StringLength(150, MinimumLength = 2)]
        
        public string SpecialLink { get; set; }

        [Required]
        [Display(Name = "Search Link")]
        [StringLength(150, MinimumLength = 2)]
        [Url]
        public string SearchLink { get; set; }

        [Required]
        [Display(Name = "Theme Color")]
        [StringLength(40, MinimumLength = 2)]
        public string Theme { get; set; }
    }
}
