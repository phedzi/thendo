﻿using SaleAI.Models;
using System.Collections.Generic;

namespace SaleAI.ViewModels
{
    public class HomeViewModel
    {
        public LoggedInUserViewModel CurrentUser { get; set; }
        public List<ShopStore> stores { get; set; }
        public List<CategoryViewModel> ProductCategories { get; set; }
        public List<ProductTypeViewModel> ProductTypes { get; set; }
        public ShopStore Cheapest { get; set; }
        public int totalQuantity { get; set; }
        public float totalAmount { get; set; }
    }
}
