﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIStore.ViewModels
{
    public class ExportProduct
    {
        public string Product { get; set; }
        public int Quantity { get; set; }
        public string  Price { get; set; }
        public string Shop { get; set; }

    }
}
