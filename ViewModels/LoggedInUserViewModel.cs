﻿
using System.Collections.Generic;

namespace SaleAI.ViewModels
{
    public class LoggedInUserViewModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string UserType { get; set; }
        public string Budget { get; set; }
    }
}
