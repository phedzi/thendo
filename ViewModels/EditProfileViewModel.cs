﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.ViewModels
{
    public class EditProfileViewModel
    {

        public int Id { get; set; }

        [Required]
        [Display (Name ="First Name")]
        [StringLength(40, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "User Name")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [StringLength(10, MinimumLength = 10)]
        public string Phone { get; set; }

        [Display(Name = "Subscription")]
        public string CreatedDate { get; set; }

        public bool UserFound { get; set; }
        public string Message { get; set; }
    }
}
