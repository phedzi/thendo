﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIStore.ViewModels
{
    public class ExportType
    {
        public string TypeName { get; set; }
        public string Extension { get; set; }
    }
}
