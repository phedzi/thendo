﻿
namespace SaleAI.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public bool RequestedPasswordReset { get; set; }
        public string PhoneNumber { get; set; }
        public string Type { get; set; }
        public bool IsSelected { get; set; }
    }
}
