﻿using System.ComponentModel.DataAnnotations;

namespace SaleAI.ViewModels
{
    public class ProductTypeViewModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string Type { get; set; }
    }
}
