﻿
namespace SaleAI.ViewModels
{
    public class ReportUserViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string CreatedAt { get; set; }
        public int UserLogInCount { get; set; }
        public bool RequestedPasswordReset { get; set; }
        public string UserTypeName { get; set; }
    }
}
