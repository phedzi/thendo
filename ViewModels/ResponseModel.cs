﻿
namespace SaleAI.ViewModels
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; } 
        public int ErrorNo { get; set; } 

        public ResponseModel()
        {
            Status = "Failed";
            Message = "Proccess failed please attend to errors ";
            ErrorNo = 403;
        }
        public ResponseModel(string status)
        {
            if( status.ToLower() == "ok")
            {
                Status = "Saved";
                Message = "Successfully Saved";
                ErrorNo = 200;
            }
        }
    }
}
