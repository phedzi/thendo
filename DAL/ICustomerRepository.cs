﻿using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface ICustomerRepository 
    {
        Task<Customer> Add(Customer model, string password);
        Task<Customer> ResetPassword(Customer model, string password);
        Task<Customer> Update(Customer model);
        Task<Customer> Delete(Customer model);
    }
}
