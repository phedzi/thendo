﻿using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface IProductTypeRepository
    {
        ProductType Get(int Id);
        ProductType GetByName(string name);
        IEnumerable<ProductType> GetAll();
        Task<ProductType> Add(ProductType model);
        ProductType Update(ProductType model);
        Task<ProductType> Delete(ProductType model);
        bool hasProduct(int id);
    }
}
