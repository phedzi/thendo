﻿using SaleAI.Models;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface IUserProductRepository
    {
        Task<UserProduct> Add(UserProduct userProd);
        Task<bool> IsProductDeleted(int prodId);
        Task<UserProduct> Update(UserProduct userProd);
        Task<UserProduct> Delete(UserProduct userProd);

    }
}
