﻿using SaleAI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface IProductRepository
    {
        Product Get(int Id);
        Product GetByProduct(Product product);
        Task<List<Product>> GetByStore(int storeId);
        IEnumerable<Product> GetAll();
        Task<IEnumerable<Product>> GetAllByUser(User user);
        Task<Product> Add(Product model);
        Product Update(Product model);
        Task<Product> Delete(Product model);
        bool AssignedToUser(UserProduct userProd);
        Task<UserProduct> DeleteUser(UserProduct model);
    }
}
