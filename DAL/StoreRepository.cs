﻿using SaleAI.Data;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class StoreRepository : IStoreRepository
    {
        private readonly StoreDBContext dbContext;

        public StoreRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        
        public async Task<Store> Add(Store model)
        {
            dbContext.TStore.Add(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public Store Get(int Id)
        {
            return dbContext.TStore

                .FirstOrDefault(u => u.StoreId == Id);
        }

        public Store GetByName(string name)
        {
            return dbContext.TStore.FirstOrDefault(s => s.Name.ToLower().Equals(name.ToLower()));
        }
        public IEnumerable<Store> GetAll()
        {
            return dbContext.TStore;
        }
        public void Update(Store model)
        {
            var _model = Get(model.StoreId);
            var Updated = dbContext.TStore.Attach(model);
            Updated.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            dbContext.SaveChanges();
        }

        public async Task<Store> Delete(Store model)
        {
            dbContext.TStore.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public IEnumerable<Store> paginateReport()
        {
            throw new NotImplementedException();
        }

    }
}
