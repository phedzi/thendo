﻿using Microsoft.EntityFrameworkCore;
using SaleAI.Data;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        private readonly StoreDBContext dbContext;

        public ProductTypeRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<ProductType> Add(ProductType model)
        {
            dbContext.TProductType.Add(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public ProductType Get(int Id)
        {
            return dbContext.TProductType.FirstOrDefault(t => t.ProductTypeId == Id);
        }

        public IEnumerable<ProductType> GetAll()
        {
            return  dbContext.TProductType;
        }

        public ProductType Update(ProductType model)
        {
            var UpdatedColor = dbContext.TProductType.Attach(model);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            dbContext.SaveChanges();

            return model;
        }

        public async Task<ProductType> Delete(ProductType model)
        {
            dbContext.TProductType.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public bool hasProduct(int id)
        {
            var product = dbContext.TProduct.Where(s => s.Type.ProductTypeId == id ).FirstOrDefault();
            return (product != null) ? true : false;
        }

        public ProductType GetByName(string name)
        {
            return dbContext.TProductType.FirstOrDefault(t => t.Type.ToLower().Trim() == name.ToLower().Trim());
        }
    }
}
