﻿using SaleAI.Models;
using SaleAI.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface IUserRepository
    {
        Task<User> Get(string username);
        Task<User> GetById(int Id);
        Task<User> ByUserName(string username);
        Task<User> GetUserWithProducts(int Id);
        IQueryable<User> GetAll();
        IQueryable<User> Filter(string filter);
        Task<bool> LogIn(LogInModel user );
        Task<User> Add(User model, string password);
        Task<User> ResetPassword(User model, string password);
        Task<User> Update(User model);
        Task<User> Delete(User model);

    }
}
