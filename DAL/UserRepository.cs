﻿using Microsoft.EntityFrameworkCore;
using SaleAI.Data;
using SaleAI.Models;
using SaleAI.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class UserRepository : IUserRepository
    {
        private readonly StoreDBContext dbContext;
        public UserRepository(
            StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task<User> Add(User model,string password)
        {
            model.CreatedAt = DateTime.Now;
            var passwordHash = PasswordHasher.Hash(password);
            model.Password = passwordHash;

            await dbContext.TUser.AddAsync(model);
            await dbContext.SaveChangesAsync();

            return model;
        }


        public IQueryable<User> GetAll()
        {
            return dbContext.TUser
                .Include(u => u.UserLogIns)
                .Where(u => u.IsDeleted == false);
        }

        public IQueryable<User> Filter(string filter)
        {
            return dbContext.TUser
                .Include(u => u.UserLogIns)
                .Where(u => u.IsDeleted == false && u.Discriminator.Equals(filter));
        }

        public async Task<User> Get(string userName)
        {
            return await dbContext.TUser.FirstOrDefaultAsync(a => a.UserName.ToLower() == userName.ToLower());
        }

        public async Task<User> GetById(int Id)
        {
            return await dbContext.TUser.FirstOrDefaultAsync(u => u.UserId == Id);
        }

        public async Task<User> GetUserWithProducts(int Id)
        {
            var u = await dbContext.TUser
                .Include(u => u.UserProducts)
                .ThenInclude(p => p.Product)
                .ThenInclude(s => s.Store)
                .SingleOrDefaultAsync(u => u.UserId == Id);
            
            return u;
        }

        public async Task<bool> LogIn(LogInModel user)
        {
            /* Fetch the stored password */
            var _user = await dbContext.TUser.FirstOrDefaultAsync(a => a.UserName.ToLower().Equals(user.UserName.ToLower()));
            return PasswordHasher.Verify(user.Password, _user.Password);
        }

        public async Task<User> Update(User model)
        {
            var UpdatedColor = dbContext.TUser.Attach(model);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await dbContext.SaveChangesAsync();

            return model;
        }

        public async Task<User> Delete(User model)
        {
            var v = dbContext.TUser.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public async Task<User> ByUserName(string username)
        {
            return await dbContext.TUser.FirstOrDefaultAsync(u => u.UserName.ToLower() == username.ToLower());
        }

        public async Task<User> ResetPassword(User model, string password)
        {
            var passwordHash = PasswordHasher.Hash(password);
            model.Password = passwordHash;
            model.RequestedPasswordReset = false;
            await Update(model);
            return model;
        }


    }
}
