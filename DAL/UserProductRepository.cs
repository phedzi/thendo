﻿using SaleAI.Data;
using SaleAI.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class UserProductRepository : IUserProductRepository
    {
        private readonly StoreDBContext dbContext;

        public UserProductRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<UserProduct> Add(UserProduct userProd)
        {
             dbContext.TUserProduct.Add(userProd);
             await dbContext.SaveChangesAsync();
            return userProd;
        }

        public async Task<UserProduct> Delete(UserProduct userProd)
        {
            dbContext.TUserProduct.Remove(userProd);
            await dbContext.SaveChangesAsync();

            return userProd;
        }

        public async Task<bool> IsProductDeleted(int prodId)
        {
            var prod = await dbContext.TUserProduct.Where(p => p.ProductId == prodId).FirstOrDefaultAsync();
            return (prod == null)? true: false;
        }

        public async Task<UserProduct> Update(UserProduct userProd)
        {
            var UpdatedColor = dbContext.TUserProduct.Attach(userProd);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            await dbContext.SaveChangesAsync();

            return userProd;
        }
    }
}
