﻿using SaleAI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface ICategoryRepository
    {
        Category Get(int Id);
        Category GetByName(string Name);
        IEnumerable<Category> GetAll();
        Task<Category> Add(Category model);
        Category Update(Category model);
        Task<Category> Delete(Category model);

        bool hasProduct(int id);
    }
}
