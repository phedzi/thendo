﻿using SaleAI.Data;
using SaleAI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly StoreDBContext dbContext;

        public CategoryRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }


        //Defined methods
        public async Task<Category> Add(Category model)
        {
            dbContext.TCategory.Add(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public Category Get(int Id)
        {
            return dbContext.TCategory.FirstOrDefault(c => c.CategoryId == Id);
        }
        public Category GetByName(string Name)
        {
            return dbContext.TCategory.FirstOrDefault(c => c.Name.ToLower() == Name.ToLower());
        }

        public IEnumerable<Category> GetAll()
        {
            return dbContext.TCategory;
        }

        public Category Update(Category model)
        {
            var UpdatedColor = dbContext.TCategory.Attach(model);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            dbContext.SaveChanges();

            return model;
        }


        public async Task<Category> Delete(Category model)
        {
            dbContext.TCategory.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }


        public bool hasProduct(int id)
        {
            var product = dbContext.TProduct.Where(s => s.Category.CategoryId == id).FirstOrDefault();
            return (product != null) ? true : false;
        }
    }
}
