﻿
using SaleAI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AIStore.DAL
{
    public interface IUserLogInRepository
    {
        IEnumerable<UserLogIn> All();
        IEnumerable<UserLogIn> UserLogIns(User user);
        Task<UserLogIn> Add(UserLogIn userLogIn);
    }
}
