﻿using SaleAI.Data;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIStore.DAL
{
    public class UserLogInRepository : IUserLogInRepository
    {
        private readonly StoreDBContext dbContext;

        public UserLogInRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<UserLogIn> Add(UserLogIn userLogIn)
        {
            userLogIn.CreatedAt = DateTime.Now;
            dbContext.TUserLogIn.Add(userLogIn);
            var prod = await dbContext.SaveChangesAsync();

            return userLogIn;
        }

        public IEnumerable<UserLogIn> All()
        {
            return dbContext.TUserLogIn;
        }

        public IEnumerable<UserLogIn> UserLogIns(User user)
        {
            return dbContext.TUserLogIn.Where(l => l.User.UserId == user.UserId);
        }
    }
}
