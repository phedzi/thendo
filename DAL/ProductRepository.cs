﻿using Microsoft.EntityFrameworkCore;
using SaleAI.Data;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class ProductRepository : IProductRepository
    {
        private readonly StoreDBContext dbContext;

        public ProductRepository (StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task<Product> Add(Product model)
        {
            dbContext.TProduct.Add(model);
            var prod = await dbContext.SaveChangesAsync();

            return model;
        }
        public async Task<IEnumerable<Product>> GetAllByUser(User user)
        {
          
            var list = await dbContext.TProduct
                .Include(p => p.Store)
                .Where(p => p.UserProducts.Where(x => x.UserId == user.UserId).FirstOrDefault().UserId == user.UserId).ToListAsync();

            return list;
        }
        public Product Get(int Id)
        {
            return dbContext.TProduct.FirstOrDefault(p => p.ProductId == Id);
        }
        public async Task<List<Product>> GetByStore(int storeId)
        {
            return await dbContext.TProduct.Where(p => p.Store.StoreId == storeId).ToListAsync();
        }
        public Product GetByProduct(Product product)
        {
            
            return dbContext.TProduct
                .FirstOrDefault(p => p.Name.Equals(product.Name) && p.Store.Name.Equals(product.Store.Name));
        }
        public IEnumerable<Product> GetAll()
        {
            return dbContext.TProduct;
        }
        public async Task<Product> Delete(Product model)
        {
            dbContext.TProduct.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }
      
        public Product Update(Product model)
        {
            var UpdatedColor = dbContext.TProduct.Attach(model);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            dbContext.SaveChanges();

            return model;
        }
        public bool AssignedToUser(UserProduct userProd)
        {
            var _product = dbContext.TProduct.Include(p => p.UserProducts)
                .FirstOrDefault(p =>p.ProductId == userProd.ProductId && 
                (p.UserProducts.FirstOrDefault(u => u.ProductId == userProd.ProductId && 
                u.UserId == userProd.UserId) != null));

            return (_product == null)? false : true;
        }

        public async Task<UserProduct> DeleteUser(UserProduct model)
        {
            var _up = dbContext.TUserProduct.SingleOrDefault(u => u.Product.ProductId == model.Product.ProductId && u.User.UserId == model.User.UserId);

            dbContext.TUserProduct.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }
    }
}
