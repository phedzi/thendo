﻿using SaleAI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public interface IStoreRepository
    {
        Store Get(int Id);
        Store GetByName(string name);
        IEnumerable<Store> GetAll();
        IEnumerable<Store> paginateReport();
        Task<Store> Add(Store model);
        void Update(Store model);
        Task<Store> Delete(Store model);
    }
}
