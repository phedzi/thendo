﻿using SaleAI.Data;
using SaleAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaleAI.DAL
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly StoreDBContext dbContext;
        public CustomerRepository(StoreDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<Customer> Add(Customer model, string password)
        {
            model.CreatedAt = DateTime.Now;
            var passwordHash = PasswordHasher.Hash(password);
            model.Password = passwordHash;

            await dbContext.TCustomer.AddAsync(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public async Task<Customer> Delete(Customer model)
        {
            var v = dbContext.TUser.Remove(model);
            await dbContext.SaveChangesAsync();

            return model;
        }

        public async Task<Customer> ResetPassword(Customer model, string password)
        {
            var passwordHash = PasswordHasher.Hash(password);
            model.Password = passwordHash;
            model.RequestedPasswordReset = false;
            await Update(model);
            return model;
        }

        public async Task<Customer> Update(Customer model)
        {
            var UpdatedColor = dbContext.TCustomer.Attach(model);
            UpdatedColor.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await dbContext.SaveChangesAsync();

            return model;
        }
    }
}
